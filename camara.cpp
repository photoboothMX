#include "camara.h"
#include <QVBoxLayout>
#include <QLabel>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <QMessageBox>
#include <QDebug>
#include <iostream>
#include <QColormap>

camara::camara(QWidget *parent) :
    QWidget(parent)
{
        QVBoxLayout* camlay = new QVBoxLayout;
        camlabe = new QLabel;
        errorid = 0;
        fondid = 0;

        camlabe->setFrameStyle(QFrame::Panel | QFrame::Raised);
        camlabe->setLineWidth(2);

        camlabe->setMinimumWidth(640);
        camlabe->setMinimumHeight(480);

        camlabe->setMaximumWidth(640);
        camlabe->setMaximumHeight(480);

        camlabe->setScaledContents(true);

        camlabe->adjustSize();

        this->setAttribute(Qt::WA_DeleteOnClose);
        this->setAttribute(Qt::WA_QuitOnClose);

        camlay->addWidget(camlabe);
        this->setLayout(camlay);

        //accesando a camara
        cap.open(0);

        if(!cap.isOpened())         //pregunta si abrio la camara bien..
      {
            QMessageBox::critical(this,tr("Problema"),tr("El programa no encuentra la camara"),
                                     QMessageBox::Yes|QMessageBox::Default);
            //que muestre una imagen de error pork no habrio la camara
            this->setpix(QPixmap(":imas/admira"));
            errorid = 1;
            emit error(errorid);
      }
}

void camara::setima(const QImage &imag)  //asigna una imagen al label principal
{
    mipix = QPixmap::fromImage(imag);
    camlabe->setPixmap(mipix);
}

void camara::setpix(const QPixmap &privpix)  //asigna una imagen tipo QPixmap al label principal
{
    mipix = privpix;
    camlabe->setPixmap(privpix);
}

QPixmap camara::getpix()    //obtiene la imagen que esta en label Principal en formato QPixmap
{
    return mipix;
}

void camara::guardImagen(const QString &nombre)
{
    if (errorid == 0)
    {
        String ruta("/tmp/");
        ruta += nombre.toStdString();
        cap >> fram;
        imwrite(ruta,fram); // tengo que aprender a guardar la QPixmap !!!

    }

}

void camara::opencam()
{
    errorid = 0;

    if(!cap.isOpened())         //pregunta si abrio la camara bien..
    {
        QMessageBox::critical(this,tr("Problema"),tr("El programa no encuentra la camara"),
                              QMessageBox::Yes|QMessageBox::Default);
        //que muestre una imagen de error pork no habrio la camara
        this->setpix(QPixmap(":imas/admira"));
        errorid = 1;
        emit error(errorid);
    }

}

void camara::newQframe() //obtiene una captura de pantalla de la camara y la guarda en QPixmap mipix
{
    if (errorid == 0)
    {
        Mat rgb;
        cap >> fram;

        cvtColor(fram, rgb, CV_BGR2RGB);
        QImage imag((const unsigned char*)(rgb.data), rgb.cols, rgb.rows, QImage::Format_RGB888);

        //cvtColor(fram, rgb, CV_BGR2GRAY);
        //QImage imag((const unsigned char*)(rgb.data), rgb.cols, rgb.rows, QImage::Format_Mono);
        //imag.invertPixels();
        //imag = imag.convertToFormat(QImage::Format_Mono);
        //imag = imag.convertToFormat(QImage::Format_MonoLSB);

        camlabe->setPixmap(QPixmap::fromImage(imag.mirrored(true,false)));
        mipix = QPixmap::fromImage(imag.mirrored(true,false));
    }

}

void camara::newQBWframe()
{
    if (errorid == 0)
    {
        Mat rgb;
        cap >> fram;
        cvtColor(fram, rgb, CV_BGR2RGB);

        QImage imag((const unsigned char*)(rgb.data), rgb.cols, rgb.rows, QImage::Format_RGB888);

        QRgb col;
            int gray;
            int width = imag.width();
            int height = imag.height();
            for (int i = 0; i < width; ++i)
            {
                for (int j = 0; j < height; ++j)
                {
                    col = imag.pixel(i, j);
                    gray = qGray(col);
                    imag.setPixel(i, j, qRgb(gray, gray, gray));
                }
            }

        //imag.invertPixels();
        camlabe->setPixmap(QPixmap::fromImage(imag.mirrored(true,false)));
        mipix = QPixmap::fromImage(imag.mirrored(true,false));
    }
}

void camara::newQINframe()
{
    if (errorid == 0)
    {
        Mat rgb;
        cap >> fram;
        cvtColor(fram, rgb, CV_BGR2RGB);

        QImage imag((const unsigned char*)(rgb.data), rgb.cols, rgb.rows, QImage::Format_RGB888);

        imag.invertPixels();
        camlabe->setPixmap(QPixmap::fromImage(imag.mirrored(true,false)));
        mipix = QPixmap::fromImage(imag.mirrored(true,false));
    }

}

void camara::newQEXTframe()
{
    if (errorid == 0)
    {
        Mat rgb;
        cap >> fram;
        cvtColor(fram, rgb, CV_BGR2RGB);

        QImage imag((const unsigned char*)(rgb.data), rgb.cols, rgb.rows, QImage::Format_RGB888);
        QImage fond(":fondos/fondo"+ QString::number(fondid));
        //QImage fond(":fondos/fondo0");
        QRgb col;
            int newpixR,newpixG,newpixB;
            int width = imag.width();
            int height = imag.height();
            //qDebug() << width << "  " << height;  (320 240)
            for (int i = 0; i < width; ++i)
            {
                for (int j = 0; j < height; ++j)
                {
                    col = imag.pixel(i, j);
                    newpixR = qRed(col);
                    newpixG = qGreen(col);
                    newpixB = qBlue(col);

                    if (((newpixR>=42) && (newpixR<=75) ) && (newpixG>50) && (newpixB>=25 && newpixB<=65)) //aqui pregunta si el rojo y azul estan en niveles bajos
                        imag.setPixel(i, j, fond.pixel(i,j));  //aqui debe poner el fondo.pixel(i,j) donde fondo es la imagen con la misma resolucion que la camara
                }
            }

        camlabe->setPixmap(QPixmap::fromImage(imag.mirrored(true,false)));
        mipix = QPixmap::fromImage(imag.mirrored(true,false));
    }
}

void camara::newQEP1frame()
{
    if (errorid == 0)
    {
        Mat rgb;
        cap >> fram;
        cvtColor(fram, rgb, CV_BGR2RGB);

        QImage imag((const unsigned char*)(rgb.data), rgb.cols, rgb.rows, QImage::Format_RGB888);

        QRgb col;
        int newpixR,newpixG,newpixB;
        int width = imag.width();
        int height = imag.height();
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                col = imag.pixel(i, j);
                newpixR = qRed(col);
                newpixG = qGreen(col);
                newpixB = qBlue(col);

                if ((newpixR >= 60) && (newpixR <= 150) && (newpixG <= 40) && (newpixG >= 30) &&
                        (newpixB >= 30) && (newpixB <= 45)) //aqui pregunta si el rojo y azul estan en niveles bajos
                    imag.setPixel(i, j, qRgb(newpixR, newpixG*2, newpixB));
            }
        }

        //imag.invertPixels();
        camlabe->setPixmap(QPixmap::fromImage(imag.mirrored(true,false)));
        mipix = QPixmap::fromImage(imag.mirrored(true,false));
    }
}

void camara::relacam()
{
    if (errorid != 1)
    errorid = 2;  //camara liberada
}

int camara::geterror()
{
    return errorid;
}

void camara::setFondidd(const int &idd)
{
    fondid=idd;
}


