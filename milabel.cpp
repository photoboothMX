#include "milabel.h"
#include <QLabel>
#include <QDebug>
#include <QVBoxLayout>

MiLabel::MiLabel(QWidget *parent) :
    QWidget(parent)
{
    label = new QLabel(this);
    label->setAttribute(Qt::WA_DeleteOnClose);

    label->setMinimumWidth(160);
    label->setMinimumHeight(120);

    label->setMaximumWidth(160);
    label->setMaximumHeight(120);

    label->setScaledContents(true);

    this->setWindowFlags(Qt::CustomizeWindowHint);

}

 void MiLabel::mousePressEvent(QMouseEvent*)
 {
    qDebug() << id;
    emit seleccionado(id);
 }

 void MiLabel::setidd(const int &num)
 {
    id = num;
 }

 void MiLabel::setPixmap(const QPixmap &pix)
 {
     mipix = pix;
     label->setPixmap(mipix);
 }

 void MiLabel::conmarco(const bool&var)
 {
     if (var==true)
     {

         label->setFrameStyle(QFrame::Panel | QFrame::Raised);
         label->setLineWidth(2);
         //label->setFrameStyle(QFrame::Raised);

     }
     else
     {
        label->setFrameStyle(QFrame::Plain);
        label->setLineWidth(0);
     }
 }

QPixmap MiLabel::getpix()
 {
    return mipix;
 }
