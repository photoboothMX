#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <QMessageBox>
#include <QImage>
#include <QDebug>
#include "camara.h"
#include <QTimer>
#include <QList>
#include <QLabel>
#include "milabel.h"
#include <QPrinter>
#include <QPainter>
#include <QSpacerItem>

// importante      ui->label->setPixmap(QPixmap::fromImage(qimg));
using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //constructor
    ui->setupUi(this);

    ui->buttonBox->hide();

    timeer = new QTimer;
    timi = new QTimer;

    //parametros fijos
    fotonum = 1;  //este sive como indice para saber en que label se musetra la foto..
    foto=0;
    //setWindowState(Qt::WindowFullScreen);
    //ui->butonFullscreen->hide();

    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Cancelar");
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("Listo!!");

    foto1 = new MiLabel();
    foto2 = new MiLabel();
    foto3 = new MiLabel();
    foto4 = new MiLabel();

    //foto1->setPixmap(QPixmap(":imas/admira"));
    foto1->setidd(-1);
    foto2->setidd(-2);
    foto3->setidd(-3);
    foto4->setidd(-4);

    foto1->setParent(ui->filmstrip);
    foto2->setParent(ui->filmstrip);
    foto3->setParent(ui->filmstrip);
    foto4->setParent(ui->filmstrip);

    foto1->setGeometry(15,14,160,120);
    foto2->setGeometry(15,159,160,120);
    foto3->setGeometry(15,303,160,120);
    foto4->setGeometry(15,447,160,120);

    //agregar fondos
    for(int i=0;i<=10;i++)
    {
        misfondos.append(new MiLabel(ui->scrollAreaFondos));
        QString pixm;
        pixm = ":fondos/fondo" + QString::number(i);
        misfondos.last()->setPixmap(pixm);
        misfondos.last()->setGeometry(5,(0 + i*120),160,120);
        misfondos.last()->conmarco(true);
        misfondos.last()->setidd(i);
        connect(misfondos.last(),SIGNAL(seleccionado(int)),this,SLOT(seSelecciono(int)));
    }

    //agrega el objeto camaru de tipo camara
    camaru = new camara;

    //acomoda los widgets en principal
    ui->verticalLayout_2->addWidget(ui->countdown);
    ui->verticalLayout_2->addWidget(camaru);
    ui->spacerCagalas->invalidate();
    ui->verticalLayout_2->addStretch();
    ui->verticalLayout_2->addWidget(ui->buttonBox);
    ui->verticalLayout_2->addWidget(ui->botonfoto);

    camaru->setpix(QPixmap(":imas/admira"));  //esto es temporal solo para que muestre alguna imagen en cuanto habra el programa
    camaru->adjustSize();

    //conecciones debe haber un if y hacer todo esto si abrio bien la camara
    connect(timeer, SIGNAL(timeout()), camaru, SLOT(newQframe()));
    timeer->start(60);

    connect(timi, SIGNAL(timeout()),this, SLOT(aumentaEnSegundo()));
    connect(this,SIGNAL(yaaa()),this,SLOT(tomafotoReal()));

    connect(foto1, SIGNAL(seleccionado(int)), this, SLOT(seSelecciono(int)));
    connect(foto2, SIGNAL(seleccionado(int)), this, SLOT(seSelecciono(int)));
    connect(foto3, SIGNAL(seleccionado(int)), this, SLOT(seSelecciono(int)));
    connect(foto4, SIGNAL(seleccionado(int)), this, SLOT(seSelecciono(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_botonfoto_clicked()  //el boton que toma la foto
{
    if (camaru->geterror() == 2)
    {
        ui->buttonBox->hide();
        camaru->opencam();
    }

    ui->countdown->setStyleSheet("color: rgb(0, 0, 0);"
                                 "border: 2px solid rgb(255, 255, 255);"
                                 "border-radius: 18px;"
                                 "padding: 15px;"
                                 "background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.113122 rgba(132, 1, 158, 255), stop:1 rgba(187, 24, 187, 128));"
                                 "min-width: 80px;"
                                 "color: rgb(255, 255, 255);"
                                 "font: 75 65pt Bitstream Charter;"
                                 );

    seg=4;              // setea seg a 6 para comenzar cuenta regresiva
    timi->start(1000);    // comienza cuenta regresiva
    ui->countdown->setText("!!Listos!!");
    ui->countdown->show();

    ui->filmstrip->blockSignals(true);
    ui->filmstrip->setDisabled(true);
    ui->printbut->blockSignals(true);
    ui->printbut->setDisabled(true);
    ui->buttonBox->hide();
    ui->herramientas->blockSignals(true);
    ui->herramientas->setDisabled(true);
    ui->botonfoto->blockSignals(true);
    ui->botonfoto->setDisabled(true);

}

void MainWindow::on_printbut_clicked()   //el boton que debe abrir un widget donde muestre la vista previa de las fotos yyy un boton para imprimirles o descartar.
{

    foto1->conmarco(false); foto2->conmarco(false); foto3->conmarco(false); foto4->conmarco(false);

    QPrinter* mipdf=new QPrinter();
    mipdf->setOutputFormat(QPrinter::PdfFormat);
    mipdf->setOutputFileName("/tmp/impresion.pdf");
    QPainter painter;

    //mipdf->setResolution(QPrinter::HighResolution);
    mipdf->setPageSize(QPrinter::Custom);
    mipdf->setFullPage(false);
    mipdf->setPageMargins(3,3,3,4,QPrinter::Millimeter);
    mipdf->setPaperSize(QSize(55,158),QPrinter::Millimeter);

    qDebug() << "page rect " << mipdf->pageRect();
    qDebug() << "paper rect " << mipdf->paperRect();

    if (! painter.begin(mipdf))
    {
        qWarning("failed to open file, is it writable?");
        painter.end();
    }else
    {
        painter.setWindow(0,0,210,600);
        ui->filmstrip->render(&painter);
    }
}

void MainWindow::on_butonFullscreen_clicked()
{
    setWindowState(Qt::WindowFullScreen);
    ui->butonFullscreen->hide();
}

void MainWindow::aumentaEnSegundo()
{
    seg--;
    ui->countdown->setText(QString::number(seg));
    qDebug() << seg;
    if (seg<=0)
        emit yaaa();
}

void MainWindow::tomafotoReal()
{

    ui->countdown->setText("");
    ui->countdown->setStyleSheet("background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.195455 rgba(1, 173, 239, 0), stop:1 rgba(45, 49, 146, 0));"
                                 "color: rgb(255, 255, 255);"
                                 "font: 75 65pt Bitstream Charter;");
    timi->stop();

    ui->filmstrip->blockSignals(false);
    ui->filmstrip->setEnabled(true);
    ui->printbut->blockSignals(false);
    ui->printbut->setEnabled(true);
    ui->buttonBox->hide();
    ui->herramientas->blockSignals(false);
    ui->herramientas->setEnabled(true);
    ui->botonfoto->blockSignals(false);
    ui->botonfoto->setEnabled(true);

    foto++;
    switch(fotonum)
    {
        case 1: {foto1->setPixmap(camaru->getpix()); camaru->guardImagen("foto" + QString::number(foto) + ".jpg");} break;
        case 2: {foto2->setPixmap(camaru->getpix()); camaru->guardImagen("foto" + QString::number(foto) + ".jpg");} break;
        case 3: {foto3->setPixmap(camaru->getpix()); camaru->guardImagen("foto" + QString::number(foto) + ".jpg");} break;
        case 4: {foto4->setPixmap(camaru->getpix()); camaru->guardImagen("foto" + QString::number(foto) + ".jpg");} break;
        default:
                {
                    QMessageBox pregunta;
                    pregunta.setText(tr("Ya no quieres las fotos?"));
                    pregunta.setInformativeText(tr("te pasaste de fotos, quieres sobre escribir las actuales?"));
                    pregunta.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
                    pregunta.setDefaultButton(QMessageBox::No);
                    pregunta.button(QMessageBox::Yes)->setText("Si");
                    pregunta.setStyleSheet("color: rgb(255, 255, 0);"
                                           "background-image: url(:/imas/fondo3);"
                                           "background-position: padding;"
                                           "background-origin: content;"
                                           "font:  75 15pt Bitstream Charter;");
                    pregunta.setParent(this);
                    int ret = pregunta.exec();
                    switch (ret){
                        case QMessageBox::Yes : fotonum=0; break;
                        default: break;
                    }
                } break;
    }
    fotonum++;

    this->seleccion(fotonum);

}

void MainWindow::seSelecciono(const int &id)
{

    switch (id)
    {
        case -1 : {camaru->setpix(foto1->getpix()); fotonum=1;} break;
        case -2 : {camaru->setpix(foto2->getpix()); fotonum=2;} break;
        case -3 : {camaru->setpix(foto3->getpix()); fotonum=3;} break;
        case -4 : {camaru->setpix(foto4->getpix()); fotonum=4;} break;
        default:  {fondoid=id;         camaru->setFondidd(fondoid); }
    }

    if (id<0)
    {
        this->seleccion(fotonum);
        camaru->relacam();
        ui->buttonBox->show();
    }

}



void MainWindow::on_buttonBox_accepted()
{
    ui->buttonBox->hide();
    fotonum++;
    camaru->opencam();
    this->seleccion(fotonum);

}

void MainWindow::on_buttonBox_rejected()
{
    ui->buttonBox->hide();
    //fotonum++;
    camaru->opencam();
    this->seleccion(fotonum);

}

void MainWindow::seleccion(const int &caso)
{
    switch (caso)
    {
        case 1 :
                {   foto1->setGeometry(15-10,14,160+15,120+15);
                    foto2->setGeometry(15,159,160,120);
                    foto3->setGeometry(15,303,160,120);
                    foto4->setGeometry(15,447,160,120);
                    foto1->conmarco(true); foto2->conmarco(false); foto3->conmarco(false); foto4->conmarco(false);
                 }  break;
        case 2 : {  foto1->setGeometry(15,14,160,120);
                    foto2->setGeometry(15-10,159,160+15,120+15);
                    foto3->setGeometry(15,303,160,120);
                    foto4->setGeometry(15,447,160,120);
                    foto1->conmarco(false); foto2->conmarco(true); foto3->conmarco(false); foto4->conmarco(false);
       }  break;
        case 3 : {  foto1->setGeometry(15,14,160,120);
                    foto2->setGeometry(15,159,160,120);
                    foto3->setGeometry(15-10,303,160+15,120+15);
                    foto4->setGeometry(15,447,160,120);
                    foto1->conmarco(false); foto2->conmarco(false); foto3->conmarco(true); foto4->conmarco(false);
       }  break;
        case 4 : {  foto1->setGeometry(15,14,160,120);
                    foto2->setGeometry(15,159,160,120);
                    foto3->setGeometry(15,303,160,120);
                    foto4->setGeometry(15-10,447,160+15,120+15);
                    foto1->conmarco(false); foto2->conmarco(false); foto3->conmarco(false); foto4->conmarco(true);
       }  break;
    }


}

//efectos

void MainWindow::on_blanegro_clicked(bool checked)
{
    if (checked==true)
    {
        //desconeccines
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQINframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEXTframe()));

        //coneccion
        connect(timeer, SIGNAL(timeout()), camaru, SLOT(newQBWframe()));

    }
}

void MainWindow::on_normal_clicked(bool checked)
{
    if (checked==true)
    {
        //desconecciones
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQBWframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQINframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEXTframe()));

        //coneccion
        connect(timeer, SIGNAL(timeout()), camaru, SLOT(newQframe()));
    }

}

void MainWindow::on_Invertido_clicked(bool checked)
{
    if (checked==true)
    {
        //desconecciones
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQBWframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEXTframe()));

        //coneccion
        connect(timeer, SIGNAL(timeout()), camaru, SLOT(newQINframe()));
    }
}

void MainWindow::on_extrafond_clicked(bool checked)
{
    if (checked==true)
    {
        //desconecciones
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQBWframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQframe()));
        disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEXTframe()));

        //coneccion
        connect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEXTframe()));
    }
}

void MainWindow::on_experimento1_clicked(bool checked)
{
        if (checked==true)
        {
            //desconecciones
            disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQBWframe()));
            disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQframe()));
            disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEXTframe()));
            disconnect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEXTframe()));

            //coneccion
            connect(timeer, SIGNAL(timeout()), camaru, SLOT(newQEP1frame()));
        }
}
