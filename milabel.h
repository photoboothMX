#ifndef MILABEL_H
#define MILABEL_H

#include <QWidget>

class QLabel;
class QPixmap;

class MiLabel : public QWidget
{
    Q_OBJECT
public:
    explicit MiLabel(QWidget *parent = 0);

private:
    QLabel* label;
    int id;
    QPixmap mipix;

protected:
    void mousePressEvent(QMouseEvent*);

signals:
    void seleccionado(const int&);

public slots:
    void setidd(const int&);
    void setPixmap(const QPixmap &);
    void conmarco(const bool&);
    QPixmap getpix();

};

#endif // MILABEL_H
