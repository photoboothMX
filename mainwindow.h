#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv/cv.h>
//#include <milabel.h>

class camara;
class QDesktopWidget;
class QLabel;
class MiLabel;
class QMessageBox;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_botonfoto_clicked();
    void on_printbut_clicked();
    void on_butonFullscreen_clicked();
    void aumentaEnSegundo();
    void tomafotoReal();
    void seSelecciono(const int&);
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void seleccion(const int&);    

    void on_blanegro_clicked(bool checked);

    void on_normal_clicked(bool checked);

    void on_Invertido_clicked(bool checked);

    void on_extrafond_clicked(bool checked);

    void on_experimento1_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    MiLabel* foto1;
    MiLabel* foto2;
    MiLabel* foto3;
    MiLabel* foto4;
    camara* camaru;
    int fotonum;
    int foto;
    QList<MiLabel*> misfondos;
    QTimer* timi;
    int seg;
    QTimer* timeer;
    int fondoid;

signals:
    void yaaa();


};

#endif // MAINWINDOW_H
