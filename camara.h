#ifndef CAMARA_H
#define CAMARA_H

#include <QWidget>
#include <opencv/cv.h>
#include <opencv/highgui.h>

using namespace cv;

class QLabel;
class QImage;
class QPixmap;

class camara : public QWidget
{
    Q_OBJECT
public:
    explicit camara(QWidget *parent = 0);
    QPixmap getpix();

private:
    QLabel* camlabe;
    QPixmap mipix;
    VideoCapture cap;
    int errorid;
    Mat fram;
    int fondid;

signals:
    void error(const int&);

public slots:
    void setima(const QImage&);
    void setpix(const QPixmap&);
    void guardImagen(const QString&);
    void newQframe();
    void newQBWframe();
    void newQINframe();
    void newQEXTframe();
    void newQEP1frame();
    void relacam();
    void opencam();
    int geterror();
    void setFondidd(const int&);
};

#endif // CAMARA_H
