#-------------------------------------------------
#
# Project created by QtCreator 2011-09-14T17:01:47
#
#-------------------------------------------------

QT       += core gui

TARGET = photobooth
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    camara.cpp \
    milabel.cpp

HEADERS  += mainwindow.h \
    camara.h \
    milabel.h
LIBS += `pkg-config opencv --cflags --libs`

FORMS    += mainwindow.ui

RESOURCES += \
    Recursos.qrc













